use async_std::{io::Cursor, task};
use clap::{crate_authors, crate_version, App, Arg};
use http::header::{HeaderName, HeaderValue};
use log::{debug, error};
use std::error::Error;
use surf::{Request as ClientRequest, Response as CResponse};
use tide::{
    middleware::{Cors, Origin, RequestLogger},
    Request, Response,
};
use url::Url;
fn main() {
    env_logger::init();

    let app = App::new("tunnel")
        .version(crate_version!())
        .author(crate_authors!())
        .about("Tunnel all the request to the tager server. Removes the 'Host' hearder.")
        .arg(
            Arg::with_name("TARGET")
                .required(true)
                .index(1)
                .help("the server to target"),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .default_value("8080")
                .help("the port the proxy server will listen"),
        )
        .arg(
            Arg::with_name("origins")
                .short("o")
                .long("origins")
                .default_value("*")
                .help("the origins to allow in CORS, multiple values should be comma seperated"),
        )
        .get_matches();

    let target = app.value_of("TARGET").unwrap().to_owned();
    let origins = match app.value_of("origins").unwrap() {
        "*" => Origin::Any,
        other => Origin::List(other.split(',').map(str::trim).map(String::from).collect()),
    };

    task::block_on(async move {
        let mut server = tide::with_state(target);
        server.middleware(RequestLogger::new());
        server.middleware(
            Cors::new()
                .allow_credentials(true)
                .allow_origin(origins)
                .allow_methods(HeaderValue::from_static("*"))
                .allow_headers(HeaderValue::from_static("Authorization, content-type")),
        );

        server.at("/").all(proxy);
        server.at("/*").all(proxy);
        let addr = format!("0.0.0.0:{}", app.value_of("port").unwrap());
        if let Err(e) = server.listen(addr).await {
            error!("{}", e);
        }
    });
}

async fn proxy(request: Request<String>) -> Response {
    let mut response = match request_to_client(request).await {
        Ok(r) => r,
        Err(e) => {
            error!("{}", e);
            return Response::new(500);
        }
    };
    let bytes = match response.body_bytes().await {
        Ok(b) => b,
        Err(e) => {
            error!("{}", e);
            return Response::new(500);
        }
    };

    let mut new_resp = Response::new(response.status().as_u16()).body(Cursor::new(bytes));

    for (name, value) in response.headers() {
        if name.starts_with("access") || name == "content-length" || name == "content-encoding" {
            debug!("filtered {{{}: {}}}", name, value);
            continue;
        }
        if let Ok(name) = name.parse::<HeaderName>() {
            debug!("from response {{{}: {}}}", name, value);
            new_resp = new_resp.set_header(name, value);
        }
    }

    new_resp
}

async fn request_to_client(mut request: Request<String>) -> Result<CResponse, Box<dyn Error>> {
    let target = format!("{}{}", request.state(), request.uri());
    let target = Url::parse(&target)?;

    let body = request.body_bytes().await?;

    let mut new_req = ClientRequest::new(request.method().clone(), target);
    new_req = new_req.body_bytes(body);
    for (k, v) in request.headers() {
        if k != "origin" && k != "referer" && k != "host" && k != "content-length" {
            debug!("from request {{{}: {}}}", k, v.to_str().unwrap());
            new_req = new_req.set_header(k, v.to_str().unwrap());
        } else {
            debug!("filterd {{{}: {}}}", k, v.to_str().unwrap());
        }
    }

    Ok(new_req.await.map_err(|e| format!("{}", e))?)
}
